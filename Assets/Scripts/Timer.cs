﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float time;
    public Text text;
    
    void Start()
    {
        StartCoroutine("Clock");
    }
    IEnumerator Clock()
    {
        while (true)
        {
            time += Time.deltaTime;
            //text.text = FormatTime(time);
            yield return null;
        }
    }

}
