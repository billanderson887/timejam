﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class DayNightCycle : MonoBehaviour {
 
    public Light sun;
    public Light moon;
    public Text TimeDisplay;

    public float secondsInFullDay = 120f;
    [Range(0,1)]
    public float currentTimeOfDay = 0;
    [HideInInspector]
    public float timeMultiplier = 1f;

    public int daysPast = 0;

    float sunInitialIntensity;
    float moonInitialIntensity;

    float dawn = 0.2f;
    float dusk = 0.8f;
    float fadeTime = 0.02f;
    bool timeProgressing;
    
    void Start() {
        sunInitialIntensity = sun.intensity;
        moonInitialIntensity = moon.intensity;
        StartCoroutine("ProgressTime");
    }
    IEnumerator ProgressTime()
    {
        timeProgressing = true;
        while (true)
        {
            UpdateSun();

            currentTimeOfDay += (Time.deltaTime / secondsInFullDay) * timeMultiplier;
    
            // Reset the time at midnight
            if (currentTimeOfDay >= 1) {
                currentTimeOfDay = 0;
                daysPast++;
            }

            TimeDisplay.text = DisplayTime(currentTimeOfDay);
            yield return null;
        }
    }
    void UpdateSun() 
    {
        // Rotate the sun based on the current time
        sun.transform.localRotation = Quaternion.Euler((currentTimeOfDay * 360f) - 90, 70, 0);
 
        float sunIntensityMultiplier = 1;
        float moonIntensityMultiplier = 0;

        // If the time of day is before dusk or after dawn:
        // No light from the sun
        // Full light from the moon
        if (currentTimeOfDay <= dawn - fadeTime || currentTimeOfDay >= dusk) {
            sunIntensityMultiplier = 0;
            moonIntensityMultiplier = 1;
        }
        // Gradually increase light before dawn
        else if (currentTimeOfDay <= dawn) {
            sunIntensityMultiplier = Mathf.Clamp01((currentTimeOfDay - (dawn - fadeTime)) * (1 / 0.02f));
        }
        // Gradually decrease light after dusk
        else if (currentTimeOfDay >= dusk - fadeTime) {
            sunIntensityMultiplier = Mathf.Clamp01(1 - ((currentTimeOfDay - (dusk - fadeTime)) * (1 / 0.02f)));
        }

        sun.intensity = sunInitialIntensity * sunIntensityMultiplier;
        moon.intensity = moonInitialIntensity * moonIntensityMultiplier;
    }

    public string DisplayTime(float time)
    {
        float t = time * 24f;
        float hours = Mathf.Floor(t);
        t *= 60;
        float minutes = Mathf.Floor(t%60);
        // Format time into military format
        string currentTime = string.Format("{0:00}:{1:00}", hours, minutes);
        // Convert to standard AM/PM format
        return DateTime.Parse(currentTime).ToString(@"hh\:mm tt");
    }

    public void SkipToNextDawn()
    {
        if (currentTimeOfDay > dawn)
        {
            daysPast++;
        }
        currentTimeOfDay = dawn;
        UpdateSun();
    }

    public void PauseTime()
    {
        if (timeProgressing)
        {
            StopCoroutine("ProgressTime");
            timeProgressing = false;
        }
    }

    public void ResumeTime()
    {
        if (!timeProgressing)
        {
            StartCoroutine("ProgressTime");
        }
    }
}